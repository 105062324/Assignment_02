var game_point = 0;
var player_data = [];

var start_menu = {
    preload:function(){
        game.load.image('wall', 'assets/wall.png');
        game.load.image('ceiling', 'assets/ceiling.png');
        this.keyboard = game.input.keyboard.addKeys({
			'space': Phaser.Keyboard.SPACEBAR,
			'q': Phaser.Keyboard.Q
		});
    },
    create:function(){
        game.stage.backgroundColor="rgb(0, 12, 107)";
		this.text_start = game.add.text(85, 160, '', {fill:'#ffdddd', fontSize: '24px', align:'center'});
		this.text_exit = game.add.text(110, 220, '', {fill:'#ffdddd', fontSize: '24px', align:'center'});

		this.ceiling = game.add.image(0, 0, 'ceiling');
		this.leftWall = game.add.sprite(0, 0, 'wall');
		this.rightWall = game.add.sprite(383, 0, 'wall');
    },
    update:function(){
        this.text_start.text = '按下 space 開始遊戲';
        this.text_exit.text = '按下 q 離開遊戲';
        
		this.check();
    },
    check:function(){
		if(this.keyboard.space.isDown){
			game.state.start('view');
		};
		if(this.keyboard.q.isDown){
			window.location.href="https://www.google.com/";
		};
	}
}

var game_view = {
    preload:function(){
		var n=0;
		firebase.database().ref("/").once('value').then(function(s){
			s.forEach(function(c){
				var k=c.val().player;
				var s;
				for(var i=0;i<k.length-3;++i){
					if(k[i]=='|'&&k[i+1]=='|'&&k[i+2]=='|'&&k[i+3]=='|'){
						s=i;
						break;
					}
				}
				var na=k.slice(0,s);
				var po=k.slice(s+4,k.length);
				player_data[n++]=[na,Number(po)];
			});
		});

        game.load.crossOrigin = 'anonymous';
		game.load.spritesheet('player', 'assets/player.png', 32, 32);
		game.load.image('wall', 'assets/wall.png');
		game.load.image('ceiling', 'assets/ceiling.png');
		game.load.image('normal', 'assets/normal.png');
		game.load.image('nails', 'assets/nails.png');
		game.load.spritesheet('conveyorRight', 'assets/conveyor_right.png', 96, 16);
		game.load.spritesheet('conveyorLeft', 'assets/conveyor_left.png', 96, 16);
		game.load.spritesheet('trampoline', 'assets/trampoline.png', 96, 22);
        game.load.spritesheet('fake', 'assets/fake.png', 96, 36);
        game.load.audio('bounce','assets/bounce.mp3');
		game.load.audio('rotate','assets/rotate.mp3');
		game.load.audio('drop','assets/drop.mp3');
        game.load.audio('hurt','assets/hurt.mp3');
        game.load.audio('step','assets/step.mp3');
    },
    create:function(){
        this.keyboard = game.input.keyboard.addKeys({
			'left': Phaser.Keyboard.LEFT,
			'right': Phaser.Keyboard.RIGHT
        });

        this.bounce = game.add.audio('bounce');
		this.rotate = game.add.audio('rotate');
		this.drop = game.add.audio('drop');
		this.hurt = game.add.audio('hurt');
		this.step = game.add.audio('step');
		
		game.stage.backgroundColor = "rgb(0, 12, 107)";
		this.ceiling = game.add.image(0, 0, 'ceiling');

		this.leftWall = game.add.sprite(0, 0, 'wall');
		game.physics.arcade.enable(this.leftWall);
		this.leftWall.body.immovable = true;

		this.rightWall = game.add.sprite(383, 0, 'wall');
		game.physics.arcade.enable(this.rightWall);
		this.rightWall.body.immovable = true;

		this.Create_player();
		this.status='running';
		this.platform_distance = 0;
		
		this.HP = game.add.text(310, 15, '', {fill:'#ffdddd', fontSize: '24px', align:'center'});
		this.score = game.add.text(20, 15, '', {fill:'#ffdddd', fontSize: '24px', align:'center'});
    },
    Create_player:function(){
		this.player = game.add.sprite(200, 50, 'player');
		this.player.anchor.setTo(0.5, 0.5);
		game.physics.arcade.enable(this.player);
		this.player.body.gravity.y = 400;
		this.player.animations.add('left', [0, 1, 2, 3], 12, true);
		this.player.animations.add('right', [9, 10, 11, 12], 12, true);
		this.player.animations.add('flyleft', [18, 19, 20, 21], 12, true);
		this.player.animations.add('flyright', [27, 28, 29, 30], 12, true);
		this.player.animations.add('fly', [36, 37, 38, 39], 12, true);
		this.player.life = 10;
		this.player.Invincible_time = 0;
		this.player.touchOn = undefined;
	},
    update:function(){
        if(this.status != 'running') return;

		game.physics.arcade.collide(this.player, this.platform_array, this.effect, null, this);
        game.physics.arcade.collide(this.player, this.leftWall);
        game.physics.arcade.collide(this.player, this.rightWall);
		this.Touch_Ceiling(this.player);
		this.Game_Over();
		this.Update_Player();
		this.Update_Platforms();
		this.Update_TextsBoard();
		this.Create_Platforms();
	},
    Touch_Ceiling:function(){
		if(this.player.body.y < 0){
			if(this.player.body.velocity.y < 0){
				this.player.body.velocity.y = 60;
			}
			if(this.player.body.velocity.y == 0){
				this.player.body.y += 16; 
			}
			if(game.time.now > this.player.Invincible_time){
                this.player.life -= 3;
                game.camera.flash(0xff0000, 70);
                this.player.Invincible_time = game.time.now + 30;
                //if(this.player.life > 0) 
                this.hurt.play();
			}
		}
    },
    Game_Over:function(){
		if(this.player.life <= 0 || this.player.body.y > 420) {
			if(this.player.body.y > 420) this.drop.play();
			if(this.player.life <= 0) this.hurt.play();
			this.game_over();
		}
    },
    game_over:function(){
		this.platform_array.forEach(function(s) {s.destroy()});
		this.platform_array = [];
		game.state.start('over');
    },
    Update_Player:function(){
		if(this.keyboard.left.isDown){
			if( (this.platform_distance/50)%2 < 1 )
				this.player.body.velocity.x = -240;
			else if( (this.platform_distance/50)%2 >= 1 )
				this.player.body.velocity.x = 240;
			else 
				this.player.body.velocity.x = 0;
		}else if(this.keyboard.right.isDown){
			if( (this.platform_distance/50)%2 < 1 )
				this.player.body.velocity.x = 240;
			else if( (this.platform_distance/50)%2 >= 1 )
				this.player.body.velocity.x = -240;
			else 
				this.player.body.velocity.x = 0;
        }else{
			this.player.body.velocity.x = 0;
		}
		if(this.player.body.velocity.y > 240) this.player.body.velocity.y = 240;
		this.Player_Animate(this.player);
    },
    Player_Animate:function(){
		var x = this.player.body.velocity.x;
		var y = this.player.body.velocity.y;

		if (x < 0 && y == 0)	this.player.animations.play('left');
        if (x > 0 && y == 0)	this.player.animations.play('right');
        if (x < 0 && y != 0)	this.player.animations.play('flyleft');
		if (x > 0 && y != 0)	this.player.animations.play('flyright');
		if (x == 0 && y != 0)	this.player.animations.play('fly');
		if (x == 0 && y == 0)	this.player.frame = 8;
	},
	last_time : Number(0),
	platform_distance : Number(0),
	platform_array : Array(),
	variable : Number(0),
	Update_Platforms:function(){
		for(var i=0; i<this.platform_array.length; i++) {
			if(this.platform_array == null) break;
			var platform = this.platform_array[i];
			platform.body.position.y -= 2;
			if(platform.body.position.y <= -20) {
				platform.destroy();
				this.platform_array.splice(i, 1);
			}
		}
	},
	Create_Platforms:function(){
		if(game.time.now > this.last_time + 700) {
			this.last_time = game.time.now;
			this.Create_One_Platform();
			this.platform_distance += 1;
			if(this.platform_distance%10 == 0  && this.variable <= 15)	this.variable += 1;
			game_point = this.platform_distance;
		}
	},
	Create_One_Platform:function(){
		var platform;
		var x = Math.random()*(400 - 96 - 40) + 20;
		var y = 450;
		var rand = Math.random() * 100;

		if(rand < 15 + this.variable) {
			platform = game.add.sprite(x, y, 'nails');
			game.physics.arcade.enable(platform);
			platform.body.setSize(96, 15, 0, 15);
		}
		else if (rand < 55 - this.variable) {
			platform = game.add.sprite(x, y, 'normal');
		}
		else if (rand < 70) {
			platform = game.add.sprite(x, y, 'fake');
			platform.animations.add('turn', [0, 1, 2, 3, 4, 5, 0], 14);
		}
		else if (rand < 80) {
			platform = game.add.sprite(x, y, 'conveyorRight');
			platform.animations.add('scroll', [0, 1, 2, 3], 16, true);
			platform.play('scroll');
		}
		else if (rand < 90) {
			platform = game.add.sprite(x, y, 'conveyorLeft');
			platform.animations.add('scroll', [0, 1, 2, 3], 16, true);
			platform.play('scroll');
		}
		else {
			platform = game.add.sprite(x, y, 'trampoline');
			platform.animations.add('jump', [4, 5, 4, 3, 2, 1, 0, 1, 2, 3], 120);
			platform.frame = 3;
		}

		game.physics.arcade.enable(platform);
		platform.body.immovable = true;
		this.platform_array.push(platform);

		platform.body.checkCollision.down = false;
		platform.body.checkCollision.left = false;
		platform.body.checkCollision.right = false;
	},
	Update_TextsBoard:function(){
		this.HP.text = 'HP:' + this.player.life;
		this.score.setText('score:' + game_point);
	},
	effect:function(player,platform){
		if(platform.key == 'conveyorRight'){
			this.Conveyor_Right_Effect(player, platform);
		}
		if(platform.key == 'conveyorLeft') {
			this.Conveyor_Left_Effect(player, platform);
		}
		if(platform.key == 'trampoline') {
			this.Trampoline_Effect(player, platform);
			this.step.play();
		}
		if(platform.key == 'nails') {
			this.Nails_Effect(player, platform);
		}
		if(platform.key == 'normal') {
			this.Basic_Effect(player, platform);
		}
		if(platform.key == 'fake') {
			this.Fake_Effect(player, platform);
		}
	},
	Conveyor_Right_Effect:function(player, platform) {
		if (player.touchOn !== platform) {
			if(player.life < 10) player.life += 1;
			player.touchOn = platform;
			this.step.play();
		}
		player.body.x += 2;
	},
	Conveyor_Left_Effect:function(player, platform) {
		if (player.touchOn !== platform) {
			if(player.life < 10) player.life += 1;
			player.touchOn = platform;
			this.step.play();
		}
		player.body.x -= 2;
	},
	Trampoline_Effect:function(player, platform) {
		platform.animations.play('jump');
		if(player.life < 10) player.life += 1;
		player.body.velocity.y = -250;
	},
	Nails_Effect:function(player, platform) {
		if (player.touchOn !== platform) {
			player.life -= 3;
			player.touchOn = platform;
			game.camera.flash(0xff0000, 70);
			if(player.life > 0) this.hurt.play();
		}
	},
	Basic_Effect:function(player, platform) {
		if (player.touchOn !== platform) {
			if(player.life < 10) player.life += 1;
			player.touchOn = platform;
			this.step.play();
		}
	},
	Fake_Effect:function(player, platform) {
		if(player.touchOn !== platform) {
			platform.animations.play('turn');
			setTimeout(function() {
				platform.body.checkCollision.up = false;
			}, 100);
			player.touchOn = platform;
			this.rotate.play();
		}
	},
	restart:function(){
		this.distance = 0;
		this.variable = 0;
		this.Create_player();
		this.status = 'running';
	}
}

var game_over = {
    preload:function(){
		game.load.image('wall', 'assets/wall.png');
		game.load.image('ceiling', 'assets/ceiling.png');
		this.keyboard = game.input.keyboard.addKeys({
			'q': Phaser.Keyboard.Q,
			'space': Phaser.Keyboard.SPACEBAR,
			'u': Phaser.Keyboard.U
		});
		this.input_name = false;
    },
    create:function(){
		game.stage.backgroundColor="rgb(0, 12, 107)";
		
		this.player_score = game.add.text(60, 50, '', {fill:'#ffe20c', fontSize: '32px', align:'center'});
		
		this.text1=game.add.text(110, 100, '', {fill:'#ffc23f', fontSize: '20px', align:'center'});
		this.text2=game.add.text(110, 130, '', {fill:'#ffc23f', fontSize: '20px', align:'center'});
		this.text3=game.add.text(110, 160, '', {fill:'#ffc23f', fontSize: '20px', align:'center'});
		this.text4=game.add.text(110, 190, '', {fill:'#ffc23f', fontSize: '20px', align:'center'});
		this.text5=game.add.text(110, 220, '', {fill:'#ffc23f', fontSize: '20px', align:'center'});
		
		this.text1_=game.add.text(260, 100, '', {fill:'#ffc23f', fontSize: '20px', align:'center'});
		this.text2_=game.add.text(260, 130, '', {fill:'#ffc23f', fontSize: '20px', align:'center'});
		this.text3_=game.add.text(260, 160, '', {fill:'#ffc23f', fontSize: '20px', align:'center'});
		this.text4_=game.add.text(260, 190, '', {fill:'#ffc23f', fontSize: '20px', align:'center'});
		this.text5_=game.add.text(260, 220, '', {fill:'#ffc23f', fontSize: '20px', align:'center'});
		
		
		this.text6=game.add.text(112, 270, '', style = {fill:'#ff5050', fontSize: '16px', align:'center'});
		this.text7=game.add.text(129, 300, '', style = {fill:'#ff5050', fontSize: '16px', align:'center'});
		this.text8=game.add.text(129, 330, '', style = {fill:'#ff5050', fontSize: '16px', align:'center'});
		player_data.sort(function(a,b){
			return a[1]<b[1];
		});
		for(var i=player_data.length;i<5;++i){
			player_data[i]=['nobody',0];
		}
		
		this.leftWall = game.add.sprite(0, 0, 'wall');
		this.rightWall = game.add.sprite(383, 0, 'wall');
		this.ceiling = game.add.image(0, 0, 'ceiling');
		
		this.player_score.text = 'You got ' + game_point.toString() + ' points!';
		
		this.text1.setText(player_data[0][0].slice(0,12));
		this.text1_.setText(player_data[0][1].toString());
		this.text2.setText(player_data[1][0].slice(0,12));
		this.text2_.setText(player_data[1][1].toString());
		this.text3.setText(player_data[2][0].slice(0,12));
		this.text3_.setText(player_data[2][1].toString());
		this.text4.setText(player_data[3][0].slice(0,12));
		this.text4_.setText(player_data[3][1].toString());
		this.text5.setText(player_data[4][0].slice(0,12));
		this.text5_.setText(player_data[4][1].toString());

		/*this.text1.text = 'Name';
		this.text1_.text = '0';
		this.text2.text = 'Name';
		this.text2_.text = '0';
		this.text3.text = 'Name';
		this.text3_.text = '0';
		this.text4.text = 'Name';
		this.text4_.text = '0';
		this.text5.text = 'Name';
		this.text5_.text = '0';*/
		
		this.text6.setText('按下 space 繼續遊戲');
		this.text7.setText('按下 u 上傳成績');
		this.text8.setText('按下 q 離開遊戲');
	},
	input_name: false,
    update:function(){
		this.check();
	},
	check:function(){
		if(this.keyboard.space.isDown){
			game.state.start('view');
		};
		if(this.keyboard.q.isDown){
			window.location.href="https://www.google.com/";
		};
		if(this.keyboard.u.isDown){
			if(!this.input_name){
				var name = prompt("Input your name:");
				if(name){
					alert('data upload!');
				}
				firebase.database().ref("/").push({
					player: name + '||||' + game_point.toString()
				});
				this.input_name = true;
			}
		}
	}
}

var game = new Phaser.Game(400, 400, Phaser.AUTO, 'canvas');
game.state.add('menu', start_menu);
game.state.add('view', game_view);
game.state.add('over', game_over);
game.state.start('menu');